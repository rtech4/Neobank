﻿using Neobank.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Neobank.Data.Repositories;

internal abstract class Repository<T> where T : Entity
{
    private int _sequence = 0;

    private readonly Dictionary<int, T> _context = new();

    protected T? Get(int id)
        => _context.TryGetValue(id, out T? value) ? value : null;

    protected IEnumerable<T> GetAll()
        => _context.Values;

    protected IEnumerable<T> Find(Func<T, bool> predicate)
        => _context.Values.Where(predicate);

    protected void Add(T value)
    {
        value.Id = _sequence++;
        _context[value.Id] = value;
    }

    protected void Remove(T value)
        => _context.Remove(value.Id);
}
