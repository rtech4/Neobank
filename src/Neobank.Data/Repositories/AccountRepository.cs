﻿using Neobank.Data.Contracts;
using Neobank.Data.Models;
using System.Collections.Generic;

namespace Neobank.Data.Repositories;

internal sealed class AccountRepository : Repository<Account>, IAccountRepository
{
    public new Account? Get(int id)
        => base.Get(id);

    public new IEnumerable<Account> GetAll()
        => base.GetAll();

    public IEnumerable<Account> GetAllByUser(int userId)
        => Find(x => x.UserId == userId);

    public new void Add(Account account)
        => base.Add(account);

    public new void Remove(Account account)
        => base.Remove(account);
}
