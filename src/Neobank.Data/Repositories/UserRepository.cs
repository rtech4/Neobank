﻿using Neobank.Data.Contracts;
using Neobank.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace Neobank.Data.Repositories;

internal sealed class UserRepository : Repository<User>, IUserRepository
{
    public new User? Get(int id)
        => base.Get(id);

    public new IEnumerable<User> GetAll()
        => base.GetAll();

    public bool HasUserByEmail(string email)
        => Find(x => x.Email == email).Any();

    public new void Add(User user)
        => base.Add(user);
}
