﻿using System;

namespace Neobank.Data.Models;

public sealed class Account : Entity
{
    private decimal _balance;

    public int UserId { get; set; }

    public string AccountNumber { get; set; }

    public decimal Balance
    {
        get => _balance;
        set
        {
            if (value < 100)
            {
                throw new ArgumentException("Account cannot have less than $100");
            }

            _balance = value;
        }
    }

    public Account(int userId, decimal initialDeposit)
        => (UserId, AccountNumber, Balance) = (userId, Guid.NewGuid().ToString(), initialDeposit);
}
