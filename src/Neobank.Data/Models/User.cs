﻿namespace Neobank.Data.Models;

public sealed class User : Entity
{
    public string Firstname { get; set; }
    public string Surname { get; set; }
    public string Email { get; set; }

    public User(string firstname, string surname, string email)
        => (Firstname, Surname, Email) = (firstname, surname, email);
}
