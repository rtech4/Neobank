﻿namespace Neobank.Data.Models;

public abstract class Entity
{
    public int Id { get; set; }
}
