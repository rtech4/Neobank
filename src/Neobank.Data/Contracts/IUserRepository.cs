﻿using Neobank.Data.Models;
using System.Collections.Generic;

namespace Neobank.Data.Contracts;

public interface IUserRepository
{
    User? Get(int id);

    IEnumerable<User> GetAll();

    bool HasUserByEmail(string email);

    void Add(User user);
}
