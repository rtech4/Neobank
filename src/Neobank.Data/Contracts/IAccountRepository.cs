﻿using Neobank.Data.Models;
using System.Collections.Generic;

namespace Neobank.Data.Contracts;

public interface IAccountRepository
{
    Account? Get(int id);

    IEnumerable<Account> GetAll();

    IEnumerable<Account> GetAllByUser(int userId);

    void Add(Account account);

    void Remove(Account account);
}
