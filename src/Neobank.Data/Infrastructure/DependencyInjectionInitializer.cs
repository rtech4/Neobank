﻿using Microsoft.Extensions.DependencyInjection;
using Neobank.Data.Contracts;
using Neobank.Data.Repositories;

namespace Neobank.Data.Infrastructure;

public static class DependencyInjectionInitializer
{
    public static void Initialize(IServiceCollection services)
    {
        services.AddSingleton<IUserRepository, UserRepository>();
        services.AddSingleton<IAccountRepository, AccountRepository>();
    }
}
