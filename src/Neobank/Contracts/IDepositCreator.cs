﻿using LanguageExt.Common;
using Neobank.Models;

namespace Neobank.Contracts;

public interface IDepositCreator
{
    Result<bool> Create(DepositParameter depositParameter);
}
