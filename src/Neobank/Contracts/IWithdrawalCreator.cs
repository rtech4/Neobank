﻿using LanguageExt.Common;
using Neobank.Models;

namespace Neobank.Contracts;

public interface IWithdrawalCreator
{
    Result<bool> Create(WithdrawalParameter withdrawalParameter);
}
