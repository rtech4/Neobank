﻿using LanguageExt.Common;
using Neobank.Data.Models;
using Neobank.Models;

namespace Neobank.Contracts;

public interface IAccountCreator
{
    Result<Account> Create(AccountParameter accountParameter);
}
