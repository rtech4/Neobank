﻿using LanguageExt.Common;

namespace Neobank.Contracts;

public interface IAccountRemover
{
    Result<bool> Remove(int id);
}
