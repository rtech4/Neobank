﻿using LanguageExt.Common;
using Neobank.Data.Models;
using Neobank.Models;

namespace Neobank.Contracts;

public interface IUserCreator
{
    Result<User> Create(UserParameter userParameter);
}
