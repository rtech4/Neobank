﻿using FluentValidation;
using Neobank.Data.Contracts;
using Neobank.Models;

namespace Neobank.Validators;

internal sealed class DepositParameterValidator : AbstractValidator<DepositParameter>
{
    public DepositParameterValidator(IAccountRepository accountRepository)
    {
        RuleFor(x => x.AccountId).NotNull();
        RuleFor(x => x.Amount).NotNull().GreaterThan(0).LessThanOrEqualTo(10_000);

        RuleFor(x => x.AccountId).Must(x => accountRepository.Get(x!.Value) is not null).WithMessage(x => $"Account #{x.AccountId} not found.");
    }
}
