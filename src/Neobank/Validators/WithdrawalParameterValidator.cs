﻿using FluentValidation;
using FluentValidation.Results;
using Neobank.Data.Contracts;
using Neobank.Models;
using System.Linq;

namespace Neobank.Validators;

internal sealed class WithdrawalParameterValidator : AbstractValidator<WithdrawalParameter>
{
    public WithdrawalParameterValidator(IAccountRepository accountRepository)
    {
        RuleFor(x => x.AccountId).NotNull();
        RuleFor(x => x.Amount).NotNull().GreaterThan(0);

        RuleFor(x => x).Custom((withdrawal, context) =>
        {
            var account = accountRepository.Get(withdrawal.AccountId!.Value);
            if (account is null)
            {
                context.AddFailure(new ValidationFailure(nameof(withdrawal.AccountId), $"Account #{withdrawal.AccountId} not found."));
                return;
            }
            
            if (account.Balance - withdrawal.Amount < 100)
            {
                context.AddFailure(new ValidationFailure(nameof(withdrawal.Amount), "Account cannot have less than $100 at any time."));
                return;
            }

            if (withdrawal.Amount > accountRepository.GetAllByUser(account.UserId).Sum(x => x.Balance) * 0.9m)
            {
                context.AddFailure(new ValidationFailure(nameof(withdrawal.Amount), "User cannot withdraw more than 90% of the total balance."));
                return;
            }
        });
    }
}
