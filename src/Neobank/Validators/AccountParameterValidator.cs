﻿using FluentValidation;
using Neobank.Data.Contracts;
using Neobank.Models;

namespace Neobank.Validators;

internal sealed class AccountParameterValidator : AbstractValidator<AccountParameter>
{
    public AccountParameterValidator(IUserRepository userRepository)
    {
        RuleFor(x => x.UserId).NotNull();
        RuleFor(x => x.InitialDeposit).NotNull().InclusiveBetween(100, 10_000);

        RuleFor(x => x.UserId).Must(x => userRepository.Get(x!.Value) is not null).WithMessage(x => $"User #{x.UserId} not found.");
    }
}
