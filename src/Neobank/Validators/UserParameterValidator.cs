﻿using FluentValidation;
using Neobank.Data.Contracts;
using Neobank.Models;

namespace Neobank.Validators;

internal sealed class UserParameterValidator : AbstractValidator<UserParameter>
{
    public UserParameterValidator(IUserRepository userRepository)
    {
        RuleFor(x => x.Firstname).NotEmpty();
        RuleFor(x => x.Surname).NotEmpty();
        RuleFor(x => x.Email).NotEmpty().EmailAddress();

        RuleFor(x => x.Email).Must(x => !userRepository.HasUserByEmail(x!)).WithMessage(x => $"User already exists by email: {x.Email}.");
    }
}
