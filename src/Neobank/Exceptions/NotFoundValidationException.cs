﻿using FluentValidation;

namespace Neobank.Exceptions;

public sealed class NotFoundValidationException : ValidationException
{
    public NotFoundValidationException(string message) : base(message) { }
}
