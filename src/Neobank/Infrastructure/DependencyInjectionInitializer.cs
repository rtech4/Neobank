﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using Neobank.Contracts;
using Neobank.Data.Contracts;
using Neobank.Models;
using Neobank.Services;
using Neobank.Validators;

namespace Neobank.Infrastructure;

public static class DependencyInjectionInitializer
{
    public static void Initialize(IServiceCollection services)
    {
        services.AddScoped<IValidator<UserParameter>>(sp => new UserParameterValidator(sp.GetRequiredService<IUserRepository>()));
        services.AddScoped<IValidator<AccountParameter>>(sp => new AccountParameterValidator(sp.GetRequiredService<IUserRepository>()));
        services.AddScoped<IValidator<DepositParameter>>(sp => new DepositParameterValidator(sp.GetRequiredService<IAccountRepository>()));
        services.AddScoped<IValidator<WithdrawalParameter>>(sp => new WithdrawalParameterValidator(sp.GetRequiredService<IAccountRepository>()));

        services.AddScoped<IUserCreator, UserCreator>();
        services.AddScoped<IAccountCreator, AccountCreator>();
        services.AddScoped<IAccountRemover, AccountRemover>();
        services.AddScoped<IDepositCreator, DepositCreator>();
        services.AddScoped<IWithdrawalCreator, WithdrawalCreator>();
    }
}
