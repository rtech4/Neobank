﻿using FluentValidation;
using LanguageExt.Common;
using Neobank.Contracts;
using Neobank.Data.Contracts;
using Neobank.Data.Models;
using Neobank.Extensions;
using Neobank.Models;

namespace Neobank.Services;

internal sealed class AccountCreator : IAccountCreator
{
    private readonly IAccountRepository _accountRepository;
    private readonly IValidator<AccountParameter> _validator;

    public AccountCreator(IAccountRepository accountRepository, IValidator<AccountParameter> validator)
        => (_accountRepository, _validator) = (accountRepository, validator);

    public Result<Account> Create(AccountParameter accountParameter)
    {
        var validationResult = _validator.Validate(accountParameter);
        if (!validationResult.IsValid)
        {
            return validationResult.GetResult<Account>();
        }

        var account = new Account(accountParameter.UserId!.Value, accountParameter.InitialDeposit!.Value);
        _accountRepository.Add(account);

        return account;
    }
}
