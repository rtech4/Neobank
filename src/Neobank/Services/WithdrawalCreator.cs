﻿using FluentValidation;
using LanguageExt.Common;
using Neobank.Contracts;
using Neobank.Data.Contracts;
using Neobank.Extensions;
using Neobank.Models;

namespace Neobank.Services;

internal sealed class WithdrawalCreator : IWithdrawalCreator
{
    private readonly IAccountRepository _accountRepository;
    private readonly IValidator<WithdrawalParameter> _validator;

    public WithdrawalCreator(IAccountRepository accountRepository, IValidator<WithdrawalParameter> validator)
        => (_accountRepository, _validator) = (accountRepository, validator);

    public Result<bool> Create(WithdrawalParameter withdrawalParameter)
    {
        var validationResult = _validator.Validate(withdrawalParameter);
        if (!validationResult.IsValid)
        {
            return validationResult.GetResult<bool>();
        }

        _accountRepository.Get(withdrawalParameter.AccountId!.Value)!.Balance -= withdrawalParameter.Amount!.Value;
        return true;
    }
}
