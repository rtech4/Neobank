﻿using LanguageExt.Common;
using Neobank.Contracts;
using Neobank.Data.Contracts;
using Neobank.Exceptions;

namespace Neobank.Services;

internal sealed class AccountRemover : IAccountRemover
{
    private readonly IAccountRepository _accountRepository;

    public AccountRemover(IAccountRepository accountRepository)
        => _accountRepository = accountRepository;

    public Result<bool> Remove(int id)
    {
        var account = _accountRepository.Get(id);
        if (account is null)
        {
            return new(new NotFoundValidationException($"Account #{id} not found."));
        }

        _accountRepository.Remove(account!);
        return true;
    }
}
