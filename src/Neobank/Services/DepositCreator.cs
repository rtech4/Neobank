﻿using FluentValidation;
using LanguageExt.Common;
using Neobank.Contracts;
using Neobank.Data.Contracts;
using Neobank.Extensions;
using Neobank.Models;

namespace Neobank.Services;

internal sealed class DepositCreator : IDepositCreator
{
    private readonly IAccountRepository _accountRepository;
    private readonly IValidator<DepositParameter> _validator;

    public DepositCreator(IAccountRepository accountRepository, IValidator<DepositParameter> validator)
        => (_accountRepository, _validator) = (accountRepository, validator);

    public Result<bool> Create(DepositParameter depositParameter)
    {
        var validationResult = _validator.Validate(depositParameter);
        if (!validationResult.IsValid)
        {
            return validationResult.GetResult<bool>();
        }

        _accountRepository.Get(depositParameter.AccountId!.Value)!.Balance += depositParameter.Amount!.Value;
        return true;
    }
}
