﻿using FluentValidation;
using LanguageExt.Common;
using Neobank.Contracts;
using Neobank.Data.Contracts;
using Neobank.Data.Models;
using Neobank.Extensions;
using Neobank.Models;

namespace Neobank.Services;

internal sealed class UserCreator : IUserCreator
{
    private readonly IUserRepository _userRepository;
    private readonly IValidator<UserParameter> _validator;

    public UserCreator(IUserRepository userRepository, IValidator<UserParameter> validator)
        => (_userRepository, _validator) = (userRepository, validator);

    public Result<User> Create(UserParameter userParameter)
    {
        var validationResult = _validator.Validate(userParameter);
        if (!validationResult.IsValid)
        {
            return validationResult.GetResult<User>();
        }

        var user = new User(userParameter.Firstname!, userParameter.Surname!, userParameter.Email!);
        _userRepository.Add(user);

        return user;
    }
}
