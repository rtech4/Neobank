﻿namespace Neobank.Models;

public sealed record AccountParameter(
    int? UserId,
    decimal? InitialDeposit);
