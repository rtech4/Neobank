﻿namespace Neobank.Models;

public abstract record FundParameter(
    int? AccountId,
    decimal? Amount);
