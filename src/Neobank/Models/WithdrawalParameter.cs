﻿namespace Neobank.Models;

public sealed record WithdrawalParameter(
    int? AccountId,
    decimal? Amount) : FundParameter(AccountId, Amount);
