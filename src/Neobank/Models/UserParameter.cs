﻿namespace Neobank.Models;

public sealed record UserParameter(
    string? Firstname,
    string? Surname,
    string? Email);
