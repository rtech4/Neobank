﻿namespace Neobank.Models;

public sealed record DepositParameter(
    int? AccountId,
    decimal? Amount) : FundParameter(AccountId, Amount);
