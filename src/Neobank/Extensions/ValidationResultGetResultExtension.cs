﻿using FluentValidation;
using FluentValidation.Results;
using LanguageExt.Common;

namespace Neobank.Extensions;

public static class ValidationResultGetResultExtension
{
    public static Result<T> GetResult<T>(this ValidationResult validationResult)
        => new(new ValidationException(validationResult.Errors));
}
