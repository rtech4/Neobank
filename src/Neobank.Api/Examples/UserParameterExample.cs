﻿using Bogus;
using Neobank.Models;
using Swashbuckle.AspNetCore.Filters;

namespace Neobank.Api.Examples;

public sealed class UserParameterExample : IExamplesProvider<UserParameter>
{
    public UserParameter GetExamples()
    {
        var faker = new Faker();
        return new(faker.Person.FirstName, faker.Person.LastName, faker.Person.Email);
    }
}
