﻿using Neobank.Data.Contracts;
using Neobank.Models;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Linq;

namespace Neobank.Api.Examples;

public sealed class AccountParameterExample : IExamplesProvider<AccountParameter>
{
    private readonly IUserRepository _userRepository;

    public AccountParameterExample(IUserRepository userRepository)
        => _userRepository = userRepository;

    public AccountParameter GetExamples()
        => new(UserId: _userRepository.GetAll().OrderBy(x => Guid.NewGuid()).FirstOrDefault()?.Id ?? 0,
            InitialDeposit: new Random().Next(100, 10_000));
}
