using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Neobank.Api.Extensions;

var builder = WebApplication.CreateBuilder(args);

ValidatorOptions.Global.DefaultRuleLevelCascadeMode = CascadeMode.Stop;
ValidatorOptions.Global.DefaultClassLevelCascadeMode = CascadeMode.Stop;

Neobank.Data.Infrastructure.DependencyInjectionInitializer.Initialize(builder.Services);
Neobank.Infrastructure.DependencyInjectionInitializer.Initialize(builder.Services);

builder.Services
    .AddVersioning()
    .AddSwagger();

var app = builder.Build();
app.MapControllers();
app.AddSwaggerUI();

await app.RunAsync();
