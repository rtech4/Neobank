﻿using LanguageExt.Common;
using Microsoft.AspNetCore.Mvc;
using Neobank.Contracts;
using Neobank.Data.Contracts;
using Neobank.Data.Models;
using Neobank.Models;
using System.Collections.Generic;

namespace Neobank.Api.Controllers.V1;

/// <summary>
/// Represents a controller to manage users.
/// </summary>
[ApiController]
[Route("api/v{version:apiVersion}")]
[ApiVersion("1.0")]
public sealed class UserController : Controller
{
    /// <summary>
    /// Returns all users.
    /// </summary>
    /// <param name="userRepository">User repository to get the users.</param>
    /// <returns>Collection of users.</returns>
    [HttpGet("users")]
    public IActionResult GetUsers([FromServices] IUserRepository userRepository)
        => Ok(new Result<IEnumerable<User>>(userRepository.GetAll()));

    /// <summary>
    /// Creates a user.
    /// </summary>
    /// <param name="userCreator">Service to process the endpoint call.</param>
    /// <param name="userParameter">User to create.</param>
    /// <returns>The created user.</returns>
    [HttpPost("users")]
    public IActionResult CreateUser([FromServices] IUserCreator userCreator, UserParameter userParameter)
        => CreatedAtAction(userCreator.Create(userParameter), nameof(CreateUser), x => new { id = x.Id });
}
