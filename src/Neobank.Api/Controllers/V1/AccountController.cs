﻿using LanguageExt.Common;
using Microsoft.AspNetCore.Mvc;
using Neobank.Contracts;
using Neobank.Data.Contracts;
using Neobank.Data.Models;
using Neobank.Models;
using System.Collections.Generic;

namespace Neobank.Api.Controllers.V1;

/// <summary>
/// Represents a controller to manage accounts.
/// </summary>
[ApiController]
[Route("api/v{version:apiVersion}")]
[ApiVersion("1.0")]
public sealed class AccountController : Controller
{
    /// <summary>
    /// Returns all accounts.
    /// </summary>
    /// <param name="accountRepository">Account repository to get the accounts.</param>
    /// <returns>Collection of accounts.</returns>
    [HttpGet("accounts")]
    public IActionResult GetAccounts([FromServices] IAccountRepository accountRepository)
        => Ok(new Result<IEnumerable<Account>>(accountRepository.GetAll()));

    /// <summary>
    /// Creates an account.
    /// </summary>
    /// <param name="accountCreator">Service to process the endpoint call.</param>
    /// <param name="accountParameter">Account to create.</param>
    /// <returns>The created account.</returns>
    [HttpPost("accounts")]
    public IActionResult CreateAccount([FromServices] IAccountCreator accountCreator, AccountParameter accountParameter)
        => CreatedAtAction(accountCreator.Create(accountParameter), nameof(CreateAccount), x => new { id = x.Id });

    /// <summary>
    /// Deletes an account.
    /// </summary>
    /// <param name="accountRemover">Service to process the endpoint call.</param>
    /// <param name="id">Account ID to remove.</param>
    /// <returns>No return data.</returns>
    [HttpDelete("accounts/{id}")]
    public IActionResult DeleteAccount([FromServices] IAccountRemover accountRemover, int id)
        => NoContent(accountRemover.Remove(id));
}
