﻿using Microsoft.AspNetCore.Mvc;
using Neobank.Contracts;
using Neobank.Models;

namespace Neobank.Api.Controllers.V1;

/// <summary>
/// Represents a controller to manage funds.
/// </summary>
[ApiController]
[Route("api/v{version:apiVersion}")]
[ApiVersion("1.0")]
public sealed class FundController : Controller
{
    /// <summary>
    /// Adds money to an account.
    /// </summary>
    /// <param name="depositCreator">Service to process the endpoint call.</param>
    /// <param name="depositParameter">Deposit details to add money.</param>
    /// <returns>No return data.</returns>
    [HttpPost("deposits")]
    public IActionResult Deposit([FromServices] IDepositCreator depositCreator, DepositParameter depositParameter)
        => NoContent(depositCreator.Create(depositParameter));

    /// <summary>
    /// Transfers money from an account.
    /// </summary>
    /// <param name="withdrawalCreator">Service to process the endpoint call.</param>
    /// <param name="withdrawalParameter">Withdrawal details to transfer money.</param>
    /// <returns>No return data.</returns>
    [HttpPost("withdrawals")]
    public IActionResult Withdrawal([FromServices] IWithdrawalCreator withdrawalCreator, WithdrawalParameter withdrawalParameter)
        => NoContent(withdrawalCreator.Create(withdrawalParameter));
}
