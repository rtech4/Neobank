﻿using FluentValidation;
using LanguageExt.Common;
using Microsoft.AspNetCore.Mvc;
using Neobank.Exceptions;
using System;
using System.Collections;

namespace Neobank.Api.Controllers;

public abstract class Controller : ControllerBase
{
    public IActionResult Ok<TValue>(Result<TValue> result)
        => result.Match<IActionResult>(x => x is ICollection collection && collection.Count == 0 ? NoContent() : Ok(x),
            exception => exception switch
            {
                NotFoundValidationException => NotFound(),
                ValidationException => BadRequest(exception.Message),
                _ => Problem(exception.Message)
            });

    public IActionResult NoContent<TValue>(Result<TValue> result)
        => result.Match<IActionResult>(x => NoContent(),
            exception => exception switch
            {
                NotFoundValidationException => NotFound(),
                ValidationException => BadRequest(exception.Message),
                _ => Problem(exception.Message)
            });

    public IActionResult CreatedAtAction<TValue>(Result<TValue> result, string actionName, Func<TValue, object> routeValues)
        => result.Match(x => CreatedAtAction(actionName, routeValues(x), x),
            exception => exception is ValidationException ? BadRequest(exception.Message) : Problem(exception.Message));
}
