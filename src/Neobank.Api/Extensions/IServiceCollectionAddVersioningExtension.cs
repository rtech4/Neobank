﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.DependencyInjection;

namespace Neobank.Api.Extensions;

public static class IServiceCollectionAddVersioningExtension
{
    private const string _parameterName = "api-version";

    public static IServiceCollection AddVersioning(this IServiceCollection services)
    {
        services.AddApiVersioning(options =>
        {
            options.ReportApiVersions = true;
            options.AssumeDefaultVersionWhenUnspecified = true;
            options.DefaultApiVersion = ApiVersion.Default;
            options.ApiVersionReader = ApiVersionReader.Combine(
                new QueryStringApiVersionReader(_parameterName),
                new HeaderApiVersionReader(_parameterName),
                new MediaTypeApiVersionReader(_parameterName),
                new UrlSegmentApiVersionReader());
        });

        services.AddVersionedApiExplorer(options =>
        {
            options.GroupNameFormat = "'v'VV";
            options.SubstitutionFormat = "VV";
            options.SubstituteApiVersionInUrl = true;
        });

        return services;
    }
}
