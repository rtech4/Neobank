﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Neobank.Api.Infrastructure;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.IO;
using System.Reflection;

namespace Neobank.Api.Extensions;

public static class IServiceCollectionAddSwaggerExtension
{
    public static IServiceCollection AddSwagger(this IServiceCollection services)
    {
        services.AddSwaggerGen(delegate (SwaggerGenOptions options) {
            options.ExampleFilters();

            options.OrderActionsBy(x => x.ActionDescriptor.RouteValues["controller"]);

            var xmlFilename = $"{Assembly.GetEntryAssembly()!.GetName().Name}.xml";
            options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename), true);

            xmlFilename = $"{Assembly.GetExecutingAssembly()!.GetName().Name}.xml";
            options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename), true);
        });

        services.AddTransient<IConfigureOptions<SwaggerGenOptions>, SwaggerGenerationOptions>();

        services.AddSwaggerExamplesFromAssemblies(Assembly.GetEntryAssembly(), Assembly.GetExecutingAssembly());
        services.AddSwaggerExamples();

        return services;
    }
}
