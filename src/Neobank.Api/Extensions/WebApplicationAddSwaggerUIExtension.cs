﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace Neobank.Api.Extensions;

public static class WebApplicationAddSwaggerUIExtension
{
    public static WebApplication AddSwaggerUI(this WebApplication applicationBuilder)
    {
        //if (!applicationBuilder.Environment.IsProduction())
        {
            applicationBuilder.UseSwagger()
                .UseSwaggerUI(options =>
                {
                    options.DocumentTitle = "Neobank";
                    foreach (var desc in applicationBuilder.Services.GetRequiredService<IApiVersionDescriptionProvider>().ApiVersionDescriptions)
                    {
                        options.SwaggerEndpoint($"/swagger/{desc.GroupName}/swagger.json", $"v{desc.ApiVersion}");
                    }
                    options.DocExpansion(DocExpansion.None);
                });
        }
        return applicationBuilder;
    }
}
