﻿using FluentValidation;
using System.Diagnostics.CodeAnalysis;

namespace Neobank.Test;

[ExcludeFromCodeCoverage]
public abstract class TestClass
{
    protected readonly Fixture _fixture;

    public TestClass()
    {
        ValidatorOptions.Global.DefaultRuleLevelCascadeMode = CascadeMode.Stop;
        ValidatorOptions.Global.DefaultClassLevelCascadeMode = CascadeMode.Stop;
        _fixture = new();
    }
}
