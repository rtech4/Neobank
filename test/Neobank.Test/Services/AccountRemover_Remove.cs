﻿using Neobank.Data.Contracts;
using Neobank.Data.Models;
using Neobank.Exceptions;
using Neobank.Services;
using System.Diagnostics.CodeAnalysis;

namespace Neobank.Test.Services;

[ExcludeFromCodeCoverage]
public sealed class AccountRemover_Remove : TestClass
{
    private readonly Mock<IAccountRepository> _accountRepositoryMock = new();
    private readonly AccountRemover _sut;

    public AccountRemover_Remove()
        => _sut = new(_accountRepositoryMock.Object);

    [Theory]
    [InlineData(100)]
    public void When_existing_account_id_is_passed_then_the_account_is_removed(decimal initialDeposit)
    {
        //Arrange
        var account = new Account(_fixture.Create<int>(), initialDeposit);
        _accountRepositoryMock.Setup(m => m.Get(It.Is<int>(x => x == account.Id))).Returns(account);

        //Act
        var result = _sut.Remove(account.Id);

        //Assert
        result.IsSuccess.Should().BeTrue();
        _accountRepositoryMock.Verify(m => m.Get(account.Id), Times.Once());
        _accountRepositoryMock.Verify(m => m.Remove(account), Times.Once());
    }

    [Fact]
    public void When_nonexisting_account_id_is_passed_then_it_returns_with_error()
    {
        //Arrange
        var accountId = _fixture.Create<int>();

        //Act
        var result = _sut.Remove(accountId);

        //Assert
        result.IsFaulted.Should().BeTrue();
        _ = result.Match<bool>(result =>
            {
                result.Should().BeFalse();
                return default;
            },
            exception =>
            {
                exception.Should().BeOfType<NotFoundValidationException>();
                exception.Message.Should().Be($"Account #{accountId} not found.");
                return default;
            });

        _accountRepositoryMock.Verify(m => m.Get(accountId), Times.Once());
    }
}
