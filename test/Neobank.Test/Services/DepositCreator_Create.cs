﻿using Neobank.Data.Contracts;
using Neobank.Data.Models;
using Neobank.Models;
using Neobank.Services;
using Neobank.Validators;
using System.Diagnostics.CodeAnalysis;

namespace Neobank.Test.Services;

[ExcludeFromCodeCoverage]
public sealed class DepositCreator_Create : TestClass
{
    private readonly Mock<IAccountRepository> _accountRepositoryMock = new();
    private readonly DepositCreator _sut;

    public DepositCreator_Create()
        => _sut = new(_accountRepositoryMock.Object, new DepositParameterValidator(_accountRepositoryMock.Object));

    [Theory]
    [InlineData(100, 100)]
    [InlineData(100, 1000)]
    [InlineData(100, 10_000)]
    [InlineData(100.01, 9_999.99)]
    public void When_valid_deposit_parameter_is_passed_then_the_deposited_amount_is_added_to_the_balance(decimal initialDeposit, decimal deposit)
    {
        //Arrange
        var account = new Account(_fixture.Create<int>(), initialDeposit);
        var depositParameter = new DepositParameter(account.Id, deposit);

        _accountRepositoryMock.Setup(m => m.Get(It.Is<int>(x => x == depositParameter.AccountId))).Returns(account);

        //Act
        var result = _sut.Create(depositParameter);

        //Assert
        result.IsSuccess.Should().BeTrue();
        _accountRepositoryMock.Verify(m => m.Get(account.Id));
        account.Balance.Should().Be(initialDeposit + deposit);
    }

    [Fact]
    public void When_invalid_deposit_parameter_is_passed_then_the_deposited_amount_is_not_added_to_the_balance()
    {
        //Arrange
        var depositParameter = _fixture.Create<DepositParameter>() with { Amount = null };

        //Act
        var result = _sut.Create(depositParameter);

        //Assert
        result.IsFaulted.Should().BeTrue();
        _accountRepositoryMock.Verify(m => m.Add(It.IsAny<Account>()), Times.Never);
    }
}
