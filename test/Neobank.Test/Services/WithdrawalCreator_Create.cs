﻿using Neobank.Data.Contracts;
using Neobank.Data.Models;
using Neobank.Models;
using Neobank.Services;
using Neobank.Validators;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Neobank.Test.Services;

[ExcludeFromCodeCoverage]
public sealed class WithdrawalCreator_Create : TestClass
{
    private readonly Mock<IAccountRepository> _accountRepositoryMock = new();
    private readonly WithdrawalCreator _sut;

    public WithdrawalCreator_Create()
        => _sut = new(_accountRepositoryMock.Object, new WithdrawalParameterValidator(_accountRepositoryMock.Object));

    [Theory]
    [InlineData(101, 1)]
    [InlineData(101.01, 0.01)]
    [InlineData(1_000, 900)]
    public void When_valid_withdrawal_parameter_is_passed_then_the_amount_is_transferred_from_the_account(decimal initialDeposit, decimal amountToWithdraw)
    {
        //Arrange
        var account = new Account(_fixture.Create<int>(), initialDeposit);
        var withdrawalParameter = new WithdrawalParameter(account.Id, amountToWithdraw);

        _accountRepositoryMock.Setup(m => m.Get(It.Is<int>(x => x == withdrawalParameter.AccountId))).Returns(account);
        _accountRepositoryMock.Setup(m => m.GetAllByUser(It.Is<int>(x => x == account.UserId))).Returns(new List<Account>() { account });

        //Act
        var result = _sut.Create(withdrawalParameter);

        //Assert
        result.IsSuccess.Should().BeTrue();
        _accountRepositoryMock.Verify(m => m.Get(account.Id));
        account.Balance.Should().Be(initialDeposit - amountToWithdraw);
    }

    [Fact]
    public void When_invalid_withdrawal_parameter_is_passed_then_the_amount_is_not_transferred_from_the_account()
    {
        //Arrange
        var withdrawalParameter = _fixture.Create<WithdrawalParameter>() with { Amount = null };

        //Act
        var result = _sut.Create(withdrawalParameter);

        //Assert
        result.IsFaulted.Should().BeTrue();
        _accountRepositoryMock.Verify(m => m.Get(It.IsAny<int>()), Times.Never);
    }
}
