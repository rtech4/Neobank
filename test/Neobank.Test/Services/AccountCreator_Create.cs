﻿using Neobank.Data.Contracts;
using Neobank.Data.Models;
using Neobank.Models;
using Neobank.Services;
using Neobank.Validators;
using System.Diagnostics.CodeAnalysis;

namespace Neobank.Test.Services;

[ExcludeFromCodeCoverage]
public sealed class AccountCreator_Create : TestClass
{
    private readonly Mock<IUserRepository> _userRepositoryMock = new();
    private readonly Mock<IAccountRepository> _accountRepositoryMock = new();
    private readonly AccountCreator _sut;

    public AccountCreator_Create()
        => _sut = new(_accountRepositoryMock.Object, new AccountParameterValidator(_userRepositoryMock.Object));

    [Theory]
    [InlineData(100)]
    public void When_valid_account_parameter_is_passed_then_a_new_account_is_created(decimal initialDeposit)
    {
        //Arrange
        var user = _fixture.Create<User>();
        var accountParameter = new AccountParameter(user.Id, initialDeposit); 

        _userRepositoryMock.Setup(m => m.Get(It.Is<int>(x => x == accountParameter.UserId))).Returns(user);

        //Act
        var result = _sut.Create(accountParameter);

        //Assert
        result.IsSuccess.Should().BeTrue();
        result.IfSucc(account =>
        {
            account.UserId.Should().Be(accountParameter.UserId);
            account.Balance.Should().Be(initialDeposit);
        });

        _accountRepositoryMock.Verify(m => m.Add(It.Is<Account>(x => x.UserId == accountParameter.UserId && x.Balance == accountParameter.InitialDeposit)), Times.Once);
    }

    [Fact]
    public void When_invalid_account_parameter_is_passed_then_account_is_not_created()
    {
        //Arrange
        var accountParameter = _fixture.Create<AccountParameter>() with { InitialDeposit = null };

        //Act
        var result = _sut.Create(accountParameter);

        //Assert
        result.IsFaulted.Should().BeTrue();
        _accountRepositoryMock.Verify(m => m.Add(It.IsAny<Account>()), Times.Never);
    }
}
