﻿using Neobank.Data.Contracts;
using Neobank.Data.Models;
using Neobank.Models;
using Neobank.Services;
using Neobank.Validators;
using System.Diagnostics.CodeAnalysis;

namespace Neobank.Test.Services;

[ExcludeFromCodeCoverage]
public sealed class UserCreator_Create : TestClass
{
    private readonly Mock<IUserRepository> _userRepositoryMock = new();
    private readonly UserCreator _sut;

    public UserCreator_Create()
        => _sut = new(_userRepositoryMock.Object, new UserParameterValidator(_userRepositoryMock.Object));

    [Fact]
    public void When_valid_user_parameter_is_passed_then_a_new_user_is_created()
    {
        //Arrange
        var userParameter = _fixture.Create<UserParameter>() with
        {
            Email = $"{_fixture.Create<EmailAddressLocalPart>().LocalPart}@{_fixture.Create<DomainName>().Domain}"
        };

        _userRepositoryMock.Setup(m => m.HasUserByEmail(It.Is<string>(x => x == userParameter.Email))).Returns(false);

        //Act
        var result = _sut.Create(userParameter);

        //Assert
        result.IsSuccess.Should().BeTrue();
        result.IfSucc(user =>
        {
            user.Should().BeEquivalentTo(userParameter);
        });

        _userRepositoryMock.Verify(m => m.Add(It.Is<User>(x => x.Email == userParameter.Email)), Times.Once);
    }

    [Fact]
    public void When_invalid_user_parameter_is_passed_then_user_is_not_created()
    {
        //Arrange
        var userParameter = _fixture.Create<UserParameter>() with { Email = null };

        //Act
        var result = _sut.Create(userParameter);

        //Assert
        result.IsFaulted.Should().BeTrue();
        _userRepositoryMock.Verify(m => m.Add(It.IsAny<User>()), Times.Never);
    }
}
