# Neobank
## Tech assessment

Couple of notes:
- I assumed that an account with a balance can be removed even if the balance is more than 90% of the user's total balance.
- Account balance is not protected from possible overflow.
- No concurreny protection.
- Decimal places are not validated.
- And more...