﻿FROM mcr.microsoft.com/dotnet/sdk:7.0 as build
WORKDIR /source

EXPOSE 80

COPY . .

RUN dotnet publish "src/Neobank.Api/Neobank.Api.csproj" -c Release -o /publish

FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS runtime
WORKDIR /app

COPY --from=build /publish .

ENTRYPOINT [ "dotnet", "Neobank.Api.dll" ]
